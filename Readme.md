# 2D platformer in GODOT

A small 2D platformer taught by the video `Learn the GODOT Game Engine in 50 MINUTES` by [Zenva](https://www.youtube.com/channel/UCDJVbKWW-ThFTraRklmF1IQ).

![Example](example.gif)