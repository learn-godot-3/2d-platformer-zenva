extends AudioStreamPlayer2D

var coinSFX = preload("res://assets/audio/coin.ogg")

func play_coin_sfx():
	stream = coinSFX
	play()
